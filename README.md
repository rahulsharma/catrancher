Catrancher Angular 1.2.x app

## Steps to run
```
npm install
npm test
npm start
Browse -> http://localhost:8085/app
```

## Prerequisites
None

## Disclaimer
No library has been used to create this application, except for AngularJS.