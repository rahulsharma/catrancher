'use strict';

angular.module('myApp.home', ['ngRoute']).run(function () {
    console.log('main app started.');
})
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'home/home.html'
        });
    }])
    .controller('homeController', ['$rootScope', '$scope', 'CatRancherService', 'CatClowderService', function Catrancher($rootScope, $scope, CatRancherService, CatClowderService) {
        console.log('homeController fired');
        var vm = this;
        $rootScope.selectedArray = [];
        vm.catsData = [];
        vm.currentDate = new Date().toString().substring(0, 15);
        $rootScope.selectedCats = {obj: [], cats: []};

        CatRancherService.getCats()
            .success(function (catData) {
                vm.catsData = catData.cats;
            })
            .error(function (http, status, fnc, httpObj) {
                console.log('catData retrieval failed.', http, status, httpObj);
            });

        $rootScope.addCat = function (i) {
            if ($rootScope.selectedArray.length >= 3) {

            } else {
                $rootScope.selectedArray.push(i)
            }

            if ($rootScope.selectedArray.length > 2) {
                CatClowderService.checkClouder();
            }
        }

        $rootScope.removeCat = function (i) {
            var idx = $rootScope.selectedArray.indexOf(i);
            if (idx > -1) {
                $rootScope.selectedArray.splice(idx, 1);
            }
        }
    }])
    .directive('cat', ['$rootScope', function Cat($rootScope) {
        return {
            restrict: 'E',
            replace: false,
            template: '<img src="http://quantcats.herokuapp.com/static/cats/{{id}}.png" title="{{id}}" alt=""  ng-click="select()" ng-class="selected"/>',
            scope: {id: '@'},
            link: function (scope, elem, attrs) {
                scope.select = function () {

                    if (!scope.selected & $rootScope.selectedArray.length <= 2) {
                        scope.selected = 'selected';
                        $rootScope.addCat(attrs.id);
                    } else if (scope.selected) {
                        scope.selected = '';
                        $rootScope.removeCat(attrs.id);
                    }
                }
            }
        }
    }])
    .factory('CatRancherService', ['$http', function CatRancherService($http) {
        // interface
        var service = {
            cats: [],
            getCats: getCats
        };
        return service;

        // implementation
        function getCats() {
            return $http.get('http://quantcats.herokuapp.com/bag')
                .success(function (catsData) {
                    console.log('success');
                    service.cats = catsData.cats;
                }).
                error(function (data, status, headers, config) {
                    alert('no match');
                });
        }
    }])
    .service('CatClowderService', ['$rootScope', '$http', '$q', function CatClowderService($rootScope, $http, $q) {
        var vm = this;
        vm.clouderTrue = $q.defer();

        vm.checkClouder = function (params) {
            var c = $rootScope.selectedArray;
            $http.get('http://quantcats.herokuapp.com/clowder?cat=' + c[0] + '&cat=' + c[1] + '&cat=' + c[2]).
                success(function (data, status, headers, config) {
                    vm.clouderTrue.resolve(data);
                    var recievedData = JSON.parse(JSON.stringify(data.id));
                    var splitrecievedData = recievedData.split(",");
                    splitrecievedData.sort();
                    splitrecievedData = splitrecievedData.join(',')
                    $rootScope.selectedCats.obj.push(splitrecievedData);
                    $rootScope.selectedCats.obj.sort();

                    if ($rootScope.selectedCats.obj.length > 1) {
                        if ($rootScope.selectedCats.obj.indexOf(recievedData) > -1) {
                            alert('Sorry! You have already added this bunch.');
                        } else {
                            var cats = data.id.split(",");
                            cats.forEach(function (cat) {
                                $rootScope.selectedCats.cats.push(cat);
                            });
                            alert('Congratulations! These cats get along.');
                        }
                    } else {
                        var cats = data.id.split(",");
                        cats.forEach(function (cat) {
                            $rootScope.selectedCats.cats.push(cat);
                        });
                        alert('Congratulations! These cats get along.');
                    }
                }).
                error(function (data, status, headers, config) {
                    alert('Oops, these cats do not get along.');
                });
        }
        return vm;
    }])
    .filter('join', function () {
        return function (charArray) {
            return charArray.join('');
        };
    });


