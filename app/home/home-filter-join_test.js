describe('The join filter', function () {
    'use strict';

    beforeEach(function () {
        module('myApp.home');
    });

    it('should join char Array to a string', inject(function (joinFilter) {
        expect(joinFilter([1, "w", "t", "g"])).toBe('1wtg');
    }));
});